# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop freedesktop-mime gtk-icon-cache
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="A free word processing program similar to Microsoft Word"
HOMEPAGE="http://www.abiword.com/"
# gentoo mirror: wget fails to fetch because of a ssl certificate error
DOWNLOADS="https://www.abisource.com/downloads/${PN}/${PV}/source/${PNV}.tar.gz
    http://distfiles.gentoo.org/distfiles/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    spell
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# no linkage to boost
DEPENDENCIES="
    build:
        dev-libs/boost[>=1.40]
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/atk
        dev-libs/fribidi[>=0.10.4]
        dev-libs/glib:2[>=2.6]
        dev-libs/libgcrypt[>=1.4.5]
        dev-libs/libxml2:2.0[>=2.4.0]
        dev-libs/libxslt
        gnome-desktop/librsvg:2[>=2.16]
        media-libs/fontconfig
        media-libs/libpng:=[>=1.2]
        media-libs/libwmf[>=0.2.8]
        office-libs/goffice:0.10[>=0.10.2]
        office-libs/libgsf:1[>=1.14.9]
        office-libs/libical:=[>=0.46]
        office-libs/wv:1[>=1.2.0]
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3
        x11-libs/libX11
        x11-libs/pango
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo[>=1.3.1] )
        spell? ( app-spell/enchant:0[>=1.2.0] )
"
#collab? (
#          dev-libs/libxml2:2.0[>=2.4.0]
#          dev-libs/boost[>=1.33.1]
#          (
#            net-im/telepathy-mission-control[>=4.60]
#            net-im/telepathy-glib[>=0.7.28]
#          ) note = [ For telepathy plugin ]
#          net-im/loudmouth:1[>=1.3.2]
#          dev-libs/dbus-glib:1[>=0.70]
#          gnome-desktop/libsoup:2.4
#          dev-libs/gnutls
#        )

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-BUILD-fix-pkg-config-invocation.patch
    "${FILES}"/${PN}-3.0.2-fix-nullptr-c++98.patch
    "${FILES}"/cec2fda355b67b5b814a803c5ed128c425cbb030.patch
    "${FILES}"/${PN}-Make-abiword-build-with-libical3.-Fixes-13881.patch
)

# TODO: Collab
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-plugins="t602 docbook clarisworks wml kword hancom openwriter pdf loadbindings mswrite garble pdb applix opendocument sdw xslfo svg gimp bmp freetranslation iscii s5 babelfish opml eml wikipedia gdict passepartout google presentation urldict hrtext mif openxml goffice latex command wmf"
    --enable-print
    --enable-templates
    --disable-collab-backend-service
    --disable-collab-backend-tcp
    --disable-collab-backend-telepathy # Will not build with recent empathy.
    --disable-collab-backend-xmpp
    --disable-static
    --with-gio
    --with-goffice
    --with-icondir=/usr/share/icons
    --with-libical
    --without-gnomevfs
    --without-gtk2
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( spell )

AT_M4DIR=( . )

pkg_postrm(){
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

